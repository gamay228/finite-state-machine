#ifndef REG_C
#define REG_C

#include <stdlib.h>
#include <stdio.h>

struct reg_t;

typedef unsigned char(*func_t)(unsigned int);

typedef struct{
	unsigned int st;
	func_t fi;
	unsigned char size;
}reg_t;

reg_t* reg_malloc(){
	reg_t *r = (reg_t*)malloc(sizeof(reg_t));
	return r;
}

void reg_set_st(reg_t *r, unsigned int statue){
	r->st = statue;
}

unsigned int reg_show_st(reg_t *r){
	return r->st;
}

void reg_init(reg_t *r, func_t f, unsigned char size){
	r->size = size;
	r->st = 0;
	r->fi = f;
}

void reg_del(reg_t *r){
	free(r);
}



unsigned char reg_takt(reg_t *r){
	unsigned int res = r->st >> (r->size - 1);
	r->st = ((r->st << 1) & ((1U << r->size) - 1)) + r->fi(r->st);
	return res;
}


#endif
