#include "reg.h"

#include <stdio.h>

unsigned int glob;

unsigned char x(unsigned char i){
        return (glob>>i)&1;
}

unsigned char fi(unsigned int st){
        glob = st;
        return x(0)^x(12)^x(3)^x(10)^x(6) ^ x(4);
}

int main(int argc, char *argv[]){
	char size = 13;
	int len = (size + (1 << size)), i, j, step;
	struct reg_t *r = reg_malloc();
	reg_init(r, fi, size);
	reg_set_st(r, 2);

	unsigned char **sections = (unsigned char**)malloc(sizeof(unsigned char*)*len);
	for (i = 0; i < len; i++){
		sections[i] = NULL;
	}
	sections[0] = (unsigned char*)malloc(sizeof(unsigned char)*(len/8 + 1));
	unsigned int *zeroes = (unsigned int*)malloc(sizeof(unsigned int)*len);
	unsigned int *polys = (unsigned int*)malloc(sizeof(unsigned int)*len);
	for (i = 0; i < len; i++){
		zeroes[i] = 0;
		polys[i] = 0;
	}
	for (i = 0; i < len/8 + 1; i++){
		sections[0][i] = 0;
	}

	for (i = 0; i < len; i++){
		sections[0][i/8] ^= reg_takt(r) << (7 - (i%8));
	}
	polys[0] = 1;
	i = 0;
	while(sections[0][i] == 0){
		zeroes[0] +=8;
		i++;
	}
	j = 7;
	while(((sections[0][i] >> j) & 1) == 0){
		zeroes[0] += 1;
		j--;
	}

	printf("0:\t");
	for (i = 0; i < len; i++){
		printf("%d", (sections[0][i/8]>>(7 - i%8))&1);
	}
	printf(":");
	for (i = 0; i < 32; i++){
		printf("%d", ((polys[0]>>(31-i)) & 1));
	}
	printf(":%d\n", zeroes[0]);

	for (step = 1; step < len; step++){
		sections[step] = malloc(sizeof(unsigned char)*(len/8 + 1));
		for(i = 0; i < (len-step)/8 + 1; i++){
			sections[step][i] = (sections[step-1][i]<<1) + ((sections[step-1][i+1] & 128)?1:0);
		}
		polys[step] = polys[step-1]<<1;
		zeroes[step] = (zeroes[step-1])?zeroes[step-1]-1:0;

		while(1){
			for (i = 0; i < step; i++){
				if (zeroes[i] == zeroes[step]) break;
			}
			if (i == step) break;
			for(j = 0; j < (len-step)/8+1; j++){
				sections[step][j] ^= sections[i][j];
			}
			sections[step][j] &= ~(1<<(7 - (len-step)%8));
			polys[step] ^= polys[i];

			i = 0;
			zeroes[step] = 0;
			while(sections[step][i] == 0){
				zeroes[step] +=8;
				i++;
			}
			j = 7;
			while(((sections[step][i] >> j) & 1) == 0){
				zeroes[step] += 1;
				j--;
			}
			if (zeroes[step] > len-step) zeroes[step] = len-step;

		}
		printf("%d:\t", step);
		for (i = 0; i < len-step; i++){
			printf("%d", (sections[step][i/8]>>(7 - i%8))&1);
		}
		printf(":");
		for (i = 0; i < 32; i++){
			printf("%d",((polys[step]>>(31-i)) & 1));
			if (i%4 == 3) printf(" ");
		}
		printf(":%d:%d\n", polys[step], zeroes[step]);
		if(zeroes[step] == len-step)break;
	}

	for (i = 0; i < len; i++){
		if (sections[i]) free(sections[i]);
	}
	free(sections);
	free(polys);
	free(zeroes);
	reg_del(r);
	return 0;
}
