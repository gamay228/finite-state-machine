#ifndef REG_H
#define REG_H

#include <stdlib.h>
#include <stdio.h>

struct reg_t;

typedef unsigned char(*func_t)(unsigned int);

struct reg_t* reg_malloc();

void reg_set_st(struct reg_t*, unsigned int);

unsigned int reg_show_st(struct reg_t*);

void reg_init(struct reg_t*, func_t, unsigned char);

void reg_del(struct reg_t*);

unsigned char reg_takt(struct reg_t*);

#endif
