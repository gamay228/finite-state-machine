#include "z_element.h"
#include "element.h"
#include "field.h"
#include "z_field.h"
#include "reg.h"

#include <stdio.h>
#include <stdlib.h>

void fi(struct element_t **st, struct element_t *e, struct field_t *f){
	element_copy(st[0], e);
	field_sum(f, e, st[17], e);
	field_sum(f, e, st[1], e);
	field_sum(f, e, st[2], e);
	field_sum(f, e, st[4], e);
	field_sum(f, e, st[7], e);
	field_sum(f, e, st[9], e);
	field_sum(f, e, st[13], e);
	field_sum(f, e, st[15], e);
	field_sum(f, e, st[24], e);

}

int main(int argc, char *argv[]){
	int size = 25, mod = 2;
	int i, step, j;

	struct field_t *f = z_field_malloc();
	z_field_init(f, mod);
	struct element_t **st = (struct element_t**)malloc((size + 1)*sizeof(struct element_t*));
	for (i = 0; i < size+1; i++){
		st[i] = z_element_malloc();
		z_element_init(st[i]);
	}
	z_element_set(st[0], 0);
	z_element_set(st[1], 0);
	z_element_set(st[2], 0);
	z_element_set(st[3], 1);

	int tmp = 1;
	for (i = 0; i < size; i++, tmp*=mod);
	struct element_t ***sec = (struct element_t***)malloc((tmp+size)*sizeof(struct element_t**));
	struct reg_t *r = reg_malloc();
	reg_init(r, fi, f, st, size);

	struct element_t ***polys = (struct element_t***)malloc((tmp+size)*sizeof(struct element_t**));
	for (i = 0; i < tmp; i++){
		polys[i] = NULL;
		sec[i] = NULL;
	}
	int *zeros = (int*)malloc(sizeof(int)*(tmp+size));
	sec[0] = (struct element_t**)malloc((tmp+size)*sizeof(struct element_t*));

	for (i = 0; i < tmp + size; i++){
		sec[0][i] = z_element_malloc();
		z_element_init(sec[0][i]);
		reg_takt(r, sec[0][i]);
//		element_print((struct element_t*)sec[0][i]);
	}
	struct element_t *tmpel = z_element_malloc();
	z_element_init(tmpel);
	struct element_t *coef = z_element_malloc();
	z_element_init(coef);

//начинаем алгоритм


	polys[0] = (struct element_t**)malloc(sizeof(struct element_t*));
	polys[0][0] = z_element_malloc();
	z_element_init(polys[0][0]);
	z_element_set(polys[0][0], 1);
	zeros[0] = i = 0;
	while(field_is_zero(f, sec[0][i])){
		zeros[0]++;
		i++;
	}
	printf("0:\t");
	for (i = 0; i < tmp + size; i++){
		element_print(sec[0][i]);
	}
	printf(":");
	for (i = 0; i < 1; i++){
		element_print(polys[0][i]);
	}
	printf(":%d\n", zeros[0]);

	for(step = 1; step < tmp+size; step++){
		sec[step] = (struct element_t**)malloc((tmp+size-step)*sizeof(struct element_t*));
		for (i = 0; i < tmp+size-step; i++){
			sec[step][i] = z_element_malloc();
			z_element_init(sec[step][i]);
			element_copy(sec[step-1][i+1], sec[step][i]);
		}
		polys[step] = (struct element_t**)malloc((step+1)*sizeof(struct element_t*));
		for (i = 0; i < step + 1; i++){
			polys[step][i] = z_element_malloc();
			z_element_init(polys[step][i]);
			if(i) element_copy(polys[step-1][i-1], polys[step][i]);
		}
		zeros[step] = zeros[step-1] - 1;
		if (zeros[step] < 0) zeros[step] = 0;
		while(1){
			for (i = 0; i < step; i++){
				if (zeros[step] == zeros[i]) break;
			}
			if (i == step)break;
			field_rev(f, sec[i][zeros[i]], tmpel);
			field_neg(f, tmpel, tmpel);
			field_mul(f, tmpel, sec[step][zeros[i]], tmpel);
			element_copy(tmpel, coef);
			for (j = 0; j < tmp+size-step; j++){
				element_copy(coef, tmpel);
				field_mul(f, sec[i][j], tmpel, tmpel);
				field_sum(f, sec[step][j], tmpel, sec[step][j]);
			}
			for (j = 0; j < i+1; j++){
				element_copy(coef, tmpel);
				field_mul(f, polys[i][j], tmpel, tmpel);
				field_sum(f, polys[step][j], tmpel, polys[step][j]);
			}
			zeros[step] = j = 0;
			while(j < tmp+size - step && field_is_zero(f, sec[step][j])){
				zeros[step]++;
				j++;
			}
		}

		printf("%d:\t", step);
		for (i = 0; i < tmp + size - step; i++){
			element_print(sec[step][i]);
		}
		printf(":");
		for (i = 0; i < step+1; i++){
			element_print(polys[step][i]);
		}
		printf(":%d\n", zeros[step]);
		if (zeros[step] == tmp+size -step) break;
	}

	for (i = 0; i < size+1; i++){
		element_del(st[i]);
	}
	for(i = 0; i < step+1; i++){
		for (j = 0; (j < tmp + size - i); j++){
			element_del(sec[i][j]);
		}
		free(sec[i]);
	}
	free(sec);
	for(i = 0; i < step+1; i++){
		for (j = 0; j < i+1; j++){
			element_del(polys[i][j]);
		}
		free(polys[i]);
	}
	field_del(f);
	free(polys);
	free(zeros);
	reg_del(r);
	free(st);
	element_del(tmpel);
	element_del(coef);

}
