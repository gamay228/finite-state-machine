#ifndef ELEMENT_C
#define ELEMENT_C

#include "element_p.h"
#include <stdlib.h>

struct element_t;

void element__func(struct element_t *e){
	return;
}

void element__func1(struct element_t *e, struct element_t *c){
	return;
}

struct element_t* element_malloc(){
	return NULL;
}
void element_init(struct element_t *e){
	e->print = element__func;
	e->del = element__func;
	e->copy = element__func1;
	return;
}
void element_set(struct element_t *e){
	return;
}
void element_print(struct element_t *e){
	e->print(e);
}
void element_del(struct element_t *e){
	e->del(e);
}
void element_copy(struct element_t *e, struct element_t *c){
	e->copy(e, c);
}

#endif
