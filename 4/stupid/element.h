#ifndef ELEMENT_H
#define ELEMENT_H

struct element_t;

struct element_t* element_malloc();
void element_init(struct element_t*);
void element_set(struct element_t*);
void element_copy(struct element_t*, struct element_t*);
void element_print(struct element_t*);
void element_del(struct element_t*);

#endif
