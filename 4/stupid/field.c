#ifndef FIELD_C
#define FIELD_C

#include "element.h"
#include "field_p.h"
#include <stdlib.h>

void field__func1(struct field_t *f, struct element_t *a, struct element_t *b, struct element_t *c){
	return;
}
void field__func2(struct field_t *f, struct element_t *a, struct element_t *b){
	return;
}
char field__func3(struct field_t *f, struct element_t *a){
	return 0;
}

void field__func4(struct field_t *f){
	return;
}

struct field_t* field_malloc(){
	return NULL;
}
void field_init(struct field_t *f){
	f->sum = field__func1;
	f->mul = field__func1;
	f->neg = field__func2;
	f->rev = field__func2;
	f->zero = field__func3;
	f->one = field__func3;
	f->del = field__func4;
}
void field_sum(struct field_t *f, struct element_t *a, struct element_t *b, struct element_t *c){
	f->sum(f, a, b, c);
}
void field_mul(struct field_t *f, struct element_t *a, struct element_t *b, struct element_t *c){
	f->mul(f, a, b, c);
}
void field_neg(struct field_t *f, struct element_t *a, struct element_t *b){
	f->neg(f, a, b);
}
void field_rev(struct field_t *f, struct element_t *a, struct element_t *b){
	f->rev(f, a, b);
}
char field_is_zero(struct field_t *f, struct element_t *a){
	return f->zero(f, a);
}
char field_is_one (struct field_t *f, struct element_t *a){
	return f->one(f, a);
}
void field_del(struct field_t *f){
	f->del(f);
}
#endif
