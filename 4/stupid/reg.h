#ifndef REG_H
#define REG_H

#include "element.h"
#include "field.h"

struct reg_t;

struct reg_t* reg_malloc();
void reg_init(struct reg_t*, void(*)(struct element_t**, struct element_t*, struct field_t*), struct field_t*, struct element_t**, char);
void reg_takt(struct reg_t*, struct element_t*);
void reg_set_st(struct reg_t*, struct element_t**);
void reg_del(struct reg_t*);

#endif
