#ifndef Z_FIELD_H
#define Z_FIELD_H

struct field_t* z_field_malloc();
void z_field_init(struct field_t*, unsigned char);

#endif
