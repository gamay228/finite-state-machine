#ifndef Z_ELEMENT_H
#define Z_ELEMENT_H

struct element_t* z_element_malloc();
void z_element_init(struct element_t*);
void z_element_set(struct element_t*, unsigned char);

#endif
