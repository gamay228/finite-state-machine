#ifndef FIELD_C
#define FIELD_C

#include "element.h"
#include "z_element.h"
#include "z_element_p.h"
#include "field_p.h"
#include <stdlib.h>

struct z_field_t{
	struct field_t f;
	unsigned char mod;
};

struct field_t* z_field_malloc(){
	struct field_t *f = (struct field_t*)malloc(sizeof(struct z_field_t));
	return f;
}

void z_field__sum(struct field_t *f, struct element_t *a, struct element_t *b, struct element_t *c){
	z_element_set(c, (((struct z_element_t*)a)->v + ((struct z_element_t*)b)->v) % ((struct z_field_t *)f)->mod);
}
void z_field__mul(struct field_t *f, struct element_t *a, struct element_t *b, struct element_t *c){
	z_element_set(c, (((struct z_element_t*)a)->v * ((struct z_element_t*)b)->v) % ((struct z_field_t *)f)->mod);
}
void z_field__neg(struct field_t *f, struct element_t *a, struct element_t *b){
	z_element_set(b, ((struct z_field_t *)f)->mod - ((struct z_element_t*)a)->v);
}
void z_field__rev(struct field_t *f, struct element_t *a, struct element_t *b){
	unsigned char i = 0;
	for (i = 0; i < ((struct z_field_t *)f)->mod; i++){
		if ((i*((struct z_element_t*)a)->v)%((struct z_field_t *)f)->mod == 1){
			z_element_set(b, i);
			return;
		}
	}
	z_element_set(b, 0);
}
char z_field__zero(struct field_t *f, struct element_t *a){
	return (((struct z_element_t*)a)->v == 0);
}
char z_field__one (struct field_t *f, struct element_t *a){
	return (((struct z_element_t*)a)->v == 1);
}
void z_field__del(struct field_t *f){
	free(f);
}


void z_field_init(struct field_t *f, unsigned char mod){
	f->sum = z_field__sum;
	f->mul = z_field__mul;
	f->neg = z_field__neg;
	f->rev = z_field__rev;
	f->zero = z_field__zero;
	f->one = z_field__one;
	f->del = z_field__del;
	((struct z_field_t *)f)->mod = mod;
}
#endif
