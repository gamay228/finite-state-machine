#ifndef Z_ELEMENT_P_H
#define Z_ELEMENT_P_H

#include "element_p.h"
#include <stdlib.h>
#include <stdio.h>

struct z_element_t{
	struct element_t e;
	unsigned char v;
};

#endif
