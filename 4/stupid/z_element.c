#ifndef Z_ELEMENT_C
#define Z_ELEMENT_C

#include "element_p.h"
#include "z_element_p.h"
#include <stdlib.h>
#include <stdio.h>


void z_element__print(struct element_t *e){
	struct z_element_t *ze = (struct z_element_t*)e;
	printf("%d", ze->v);
}

void z_element__del(struct element_t *e){
	free(e);
}

void z_element__copy(struct element_t *e, struct element_t *c){
	((struct z_element_t*)c)->v = ((struct z_element_t*)e)->v;
}

struct element_t* z_element_malloc(){
	struct element_t *e = (struct element_t*)malloc(sizeof(struct z_element_t));
	return e;
}
void z_element_init(struct element_t *e){
	e->print = z_element__print;
	e->del = z_element__del;
	e->copy = z_element__copy;
	((struct z_element_t*)e)->v = 0;
}

void z_element_set(struct element_t *e, unsigned char v){
	((struct z_element_t*)e)->v = v;
}


#endif
