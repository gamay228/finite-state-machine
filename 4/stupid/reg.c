#ifndef REG_C
#define REG_C

#include "element.h"
#include "field.h"


#include <stdlib.h>

struct reg_t{
	struct element_t **st;
	struct field_t *f;
	void (*fi)(struct element_t**, struct element_t*, struct field_t*);
	char size;
};

struct reg_t* reg_malloc(){
	struct reg_t *r = (struct reg_t*)malloc(sizeof(struct reg_t));
	return r;
}

void reg_init(struct reg_t *r, void(*fi)(struct element_t**, struct element_t*, struct field_t*), struct field_t *f, struct element_t **st, char size){
	r->fi = fi;
	r->f = f;
	r->size = size;
	r->st = st;
}

void reg_takt(struct reg_t *r, struct element_t *e){
	element_copy(r->st[0], e);
	r->fi(r->st, r->st[r->size], r->f);
	for (char i = 0; i < r->size; i++){
		element_copy(r->st[i+1], r->st[i]);
	}
}

void reg_set_st(struct reg_t *r, struct element_t **st){
	for (char i = 0; i < r->size; i++){
		element_copy(st[i], r->st[i]);
	}
}

void reg_del(struct reg_t *r){
	free(r);
}

#endif
