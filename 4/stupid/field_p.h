#ifndef FIELD_P_H
#define FIELD_P_H

#include "element.h"

struct field_t{
	void (*sum )(struct field_t*, struct element_t*, struct element_t*, struct element_t*);
	void (*mul )(struct field_t*, struct element_t*, struct element_t*, struct element_t*);
	void (*neg )(struct field_t*, struct element_t*, struct element_t*);
	void (*rev )(struct field_t*, struct element_t*, struct element_t*);
	char (*zero)(struct field_t*, struct element_t*);
	char (*one )(struct field_t*, struct element_t*);
	void (*del )(struct field_t*);
};

#endif
