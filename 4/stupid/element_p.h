#ifndef ELEMENT_P_H
#define ELEMENT_P_H

struct element_t;

struct element_t{
	void (*print)(struct element_t*);
	void (*del)(struct element_t*);
	void (*copy)(struct element_t*, struct element_t*);
};

#endif
