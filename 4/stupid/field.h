#ifndef FIELD_H
#define FIELD_H

#include "element.h"

struct field_t;

struct field_t* field_malloc();
void field_init(struct field_t*);
void field_sum(struct field_t*, struct element_t*, struct element_t*, struct element_t*);
void field_mul(struct field_t*, struct element_t*, struct element_t*, struct element_t*);
void field_neg(struct field_t*, struct element_t*, struct element_t*);
void field_rev(struct field_t*, struct element_t*, struct element_t*);
char field_is_zero(struct field_t*, struct element_t*);
char field_is_one (struct field_t*, struct element_t*);
void field_del(struct field_t*);
#endif
