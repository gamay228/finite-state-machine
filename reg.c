#ifndef REG_C
#define REG_C

#include <stdlib.h>
#include <stdio.h>

typedef struct{
	unsigned int st;
	unsigned char **fi;
	unsigned char **psi;
	unsigned char size;
}reg_t;

reg_t* reg_malloc(){
	reg_t *r = (reg_t*)malloc(sizeof(reg_t));
	return r;
}

void reg_set_st(reg_t *r, unsigned int statue){
	r->st = statue;
}

unsigned int reg_show_st(reg_t *r){
	return r->st;
}

void reg_init(reg_t *r, unsigned char **f, unsigned char **p, unsigned char size){
	r->fi = (unsigned char**)malloc(2*sizeof(unsigned char*));
	r->psi = (unsigned char**)malloc(2*sizeof(unsigned char*));
	r->psi[0] = (unsigned char*)malloc((1<<size)*sizeof(unsigned char));
	r->psi[1] = (unsigned char*)malloc((1<<size)*sizeof(unsigned char));
	r->fi[0] = (unsigned char*)malloc((1<<size)*sizeof(unsigned char));
	r->fi[1] = (unsigned char*)malloc((1<<size)*sizeof(unsigned char));

	r->size = size;
	r->st = 0;
	int i = 0, max = 1<<size;
	for (i = 0; i < max; i++){
		r->fi[0][i] = f[0][i];
		r->psi[0][i] = p[0][i];
		r->fi[1][i] = f[1][i];
		r->psi[1][i] = p[1][i];
	}
}

void reg_del(reg_t *r){
	free(r->fi[0]);
	free(r->fi[1]);
	free(r->psi[0]);
	free(r->psi[1]);
	free(r->fi);
	free(r->psi);
	free(r);
}



unsigned char reg_takt(reg_t *r, unsigned char in){
	unsigned int res = r->psi[in][r->st];
	r->st = ((r->st << 1) & ((1 << r->size) - 1)) + r->fi[in][r->st];
	return res;
}

//-------------------------------------------------------------------

struct ecv_class_t;

typedef struct{
	unsigned int *sts;
	int size;
	int max_size;
	struct ecv_class_t *result[2];
	struct ecv_class_t *next;
	int index;
}ecv_class_t;

ecv_class_t* ecv_class_malloc(){
	ecv_class_t *r = (ecv_class_t*)malloc(sizeof(ecv_class_t));
	return r;
}

int ecv_calls_get_size(ecv_class_t *ec){
	return ec->size;
}

int ecv_calls_get_max_size(ecv_class_t *ec){
	return ec->max_size;
}

unsigned int ecv_class_get_st(ecv_class_t *ec, int index){
	if (index < 0 || index > ec->size - 1) return -1;
	return ec->sts[index];
}

void ecv_class_add_st(ecv_class_t *ec, unsigned int st){
	if (ec->size == ec->max_size){
		ec->sts = (unsigned int *)realloc(ec->sts, (ec->max_size * 2 + 10) * sizeof(unsigned int));
		ec->max_size = ec->max_size * 2 + 10;
	}
	ec->sts[ec->size] = st;
	ec->size++;
}

void ecv_class_init(ecv_class_t *ec){
	ec->sts = (unsigned int*)malloc(sizeof(unsigned int));
	ec->size = 0;
	ec->max_size = 1;
	ec->next = NULL;
	ec->result[0] = NULL;
	ec->result[1] = NULL;
}

void ecv_class_list_add(ecv_class_t **head, ecv_class_t **tail, ecv_class_t *el){
	if (!(*head)){
		*head = el;
		*tail = el;
		return;
	}
	(*tail)->next = (struct ecv_class_t*)el;
	*tail = el;
}

void ecv_class_del(ecv_class_t *head){
	ecv_class_t *tmp = head;
	while(head){
		tmp = (ecv_class_t*)head->next;
		free(head->sts);
		free(head);
		head = tmp;
	}
}

void reg_find_memory(reg_t*, ecv_class_t*, ecv_class_t*, ecv_class_t**, int);

void reg_find_min(reg_t *r){
	ecv_class_t *now_head = NULL, *now_tail = NULL, *next_head = NULL, *next_tail = NULL, *tmp, **now, *step, *tmp_next_head = NULL, *tmp_next_tail = NULL;
	int i = 0, max = 1<<r->size, num = 1, now_class_count, next_class_count, stupid_c = 0, count = 0;
	ecv_class_t *res[2];
	now = (ecv_class_t**)malloc(sizeof(ecv_class_t*) * (1 << r->size));
	now_class_count = 1;
	next_class_count = 0;
	for (i = 0; i < max; i++){
		reg_set_st(r, i);
		res[0] = (struct ecv_class_t*)reg_takt(r, 0);
		reg_set_st(r, i);
		res[1] = (struct ecv_class_t*)reg_takt(r, 1);
		tmp = now_head;
		while(tmp){
			if (tmp->result[0] == res[0] && tmp->result[1] == res[1]){
				ecv_class_add_st(tmp, i);
				now[i] = tmp;
				break;
			}
			tmp = (ecv_class_t*)tmp->next;
		}
		if (!tmp){
			tmp = ecv_class_malloc();
			ecv_class_init(tmp);
			tmp->result[0] = res[0];
			tmp->result[1] = res[1];
			ecv_class_list_add(&now_head, &now_tail, tmp);
			next_class_count++;
			ecv_class_add_st(tmp, i);
			now[i] = tmp;
		}
	}
	stupid_c++;
	printf("%d:\n", stupid_c);
	tmp = now_head;

	while(tmp){
		printf("{");
		for (i = 0; i < tmp->size; i++){
			printf("%x, ", tmp->sts[i]);
		}
		printf("}\n");
		tmp = (ecv_class_t*)tmp->next;
	}
	printf("--------------------------------------------\n");

	while((now_class_count != next_class_count) && (next_class_count != max)){
		now_class_count = next_class_count;
		next_class_count = 0;
		step = now_head;
		while(step){
			for(i = 0; i < step->size; i++){
				reg_set_st(r, step->sts[i]);
				reg_takt(r, 0);
				res[0] = now[reg_show_st(r)];
				reg_set_st(r, step->sts[i]);
				reg_takt(r, 1);
				res[1] = now[reg_show_st(r)];
				tmp = tmp_next_head;
				while(tmp){
					if (tmp->result[0] == res[0] && tmp->result[1] == res[1]){
						ecv_class_add_st(tmp, step->sts[i]);
						break;
					}
					tmp = (ecv_class_t*)tmp->next;
				}
				if (!tmp){
					tmp = ecv_class_malloc();
					ecv_class_init(tmp);
					tmp->result[0] = res[0];
					tmp->result[1] = res[1];
					ecv_class_list_add(&tmp_next_head, &tmp_next_tail, tmp);
					ecv_class_add_st(tmp, step->sts[i]);
					next_class_count++;
				}
			}
			tmp = tmp_next_head;
			while(tmp){
				ecv_class_list_add(&next_head, &next_tail, tmp);
				tmp = (ecv_class_t*)tmp->next;
			}
			tmp_next_head = tmp_next_tail = NULL;
			step = (ecv_class_t*)step->next;
		}
		ecv_class_del(now_head);
		now_head = next_head;
		now_tail = next_tail;
		tmp = now_head;
		while(tmp){
			for (i = 0; i < tmp->size; i++){
				now[tmp->sts[i]] = tmp;
			}
			tmp = (ecv_class_t*)tmp->next;
		}
		next_head = next_tail = NULL;

		stupid_c++;
		printf("%d:\n", stupid_c);
		tmp = now_head;
		count = 0;
		while(tmp){
			count++;
			printf("{");
			for (i = 0; i < tmp->size; i++){
				printf("%x, ", tmp->sts[i]);
			}
			printf("}\n");
			tmp = (ecv_class_t*)tmp->next;
		}
		printf("--------------------------------------------%d\n", count);
	}
//	reg_find_memory(r, now_head, now_tail, now, next_class_count);
	free(now);
	ecv_class_del(now_head);
}

struct list_t;

struct list_t{
	unsigned int x;
	unsigned int y;
	struct list_t *next;
};

void list_print(struct list_t *l){
	struct list_t *tmp = l;
	printf("x:");
	while(tmp){
		printf("%x", tmp->x);
		tmp = tmp->next;
	}
	printf("\ny:");
	tmp = l;
	while(tmp){
		printf("%x", tmp->y);
		tmp = tmp->next;
	}
	printf("\n");
}

struct list_t* list_malloc(){
	struct list_t *l = (struct list_t*)malloc(sizeof(struct list_t));
	l->x = 0;
	l->y = 0;
	l->next = NULL;
	return l;
}

void list_del(struct list_t *l){
	struct list_t *tmp = l;
	while(tmp){
		tmp = l->next;
		free(l);
		l = tmp;
	}
}

char list_compare(struct list_t *l, struct list_t *c){
	while(l){
		if (l->x == c->x){
			if (l->y == c->y){
				l = l->next;
				c = c->next;
			}
			else return (l->y < c->y)?-1:1;
		}
		else return (l->x < c->x)?-1:1;
	}
	return 0;
}

void list_add(struct list_t *l, char x, char y, int index){
	while(index >= 32){
		if (l->next) l = l->next;
		else{
			l->next = list_malloc();
			l = l->next;
		}
		index = index - 32;
	}
	if(x) l->x |= ((unsigned int)1)<<(31 - index);
	if(y) l->y |= ((unsigned int)1)<<(31 - index);
}

struct list_t* list_copy(struct list_t *l){
	struct list_t *r = list_malloc(), *tmp;
	tmp = r;
	while(l){
		tmp->x = l->x;
		tmp->y = l->y;
		l = l->next;
		if (l){
			tmp->next = list_malloc();
			tmp = tmp->next;
		}
	}
	return r;
}

struct statue_list_t;

struct statue_list_t{
	int index;
	struct statue_list_t *next;
};

struct statue_list_t* statue_list_malloc(){
	struct statue_list_t *r = (struct statue_list_t*)malloc(sizeof(struct statue_list_t));
	r->next = NULL;
	return r;
}

struct statue_list_t* statue_list_add(struct statue_list_t *s, struct statue_list_t *n){
	n->next = s;
	return n;
}

void statue_list_del(struct statue_list_t *s){
	struct statue_list_t *tmp;
	while(s){
		tmp = s->next;
		free(s);
		s = tmp;
	}
}

struct element_t;

struct element_t{
	struct element_t *par;
	struct list_t *el;
	struct element_t *left;
	struct element_t *right;
	ecv_class_t *from;
	int hight;
	struct statue_list_t *sts;
};

struct element_t *element_malloc(){
	struct element_t *e = (struct element_t*)malloc(sizeof(struct element_t));
	e->el = list_malloc();
	e->sts = NULL;
	e->left = NULL;
	e->right = NULL;
	e->par = NULL;
	e->from = NULL;
	e->hight = 1;
	return e;
}

char element_compare(struct element_t *e, struct element_t *c){
	return list_compare(e->el, c->el);
}

struct element_t* element_copy(struct element_t *e){
	if (!e) return NULL;
	struct element_t *r = element_malloc();
	list_del(r->el);
	r->el = list_copy(e->el);
	return r;
}

void element_add_step(struct element_t *e, char x, char y, int index){
	if (!e) e = element_malloc();
	list_add(e->el, x, y, index);
}

void element_add_st(struct element_t *e, int i){
	struct statue_list_t *tmp = statue_list_malloc();
	tmp->index = i;
	e->sts = statue_list_add(e->sts, tmp);
}

int element_get_hight(struct element_t *e){
	return(e)?e->hight:0;
}

int element_get_balance(struct element_t *e){
	if(!e) return 0;
	return element_get_hight(e->right) - element_get_hight(e->left);
}

void element_fix_hight(struct element_t *e){
	int a = element_get_hight(e->right), b = element_get_hight(e->left);
	e->hight = ((a > b)?a:b) + 1;
}

struct element_t* element_right_rotate(struct element_t *p){
	struct element_t *q = p->left;
	p->left = q->right;
	q->right = p;
	if (p->left) p->left->par = p;
	q->par = p->par;
	p->par = q;
	element_fix_hight(p);
	element_fix_hight(q);
	return q;
}

struct element_t* element_left_rotate(struct element_t *p){
	struct element_t *q = p->right;
	p->right = q->left;
	q->left = p;
	if (p->right) p->right->par = p;
	q->par = p->par;
	p->par = q;
	element_fix_hight(p);
	element_fix_hight(q);
	return q;
}

struct element_t* element_balance(struct element_t *p){
	element_fix_hight(p);
	if (element_get_balance(p) == 2){
		if (element_get_balance(p->right) < 0){
			p->right = element_right_rotate(p->right);
		}
		p = element_left_rotate(p);
	}
	if (element_get_balance(p) == -2){
		if (element_get_balance(p->left) > 0){
			p->left = element_left_rotate(p->left);
		}
		p = element_right_rotate(p);
	}
	return p;
}

struct element_t* element_insert(struct element_t *p, struct element_t *n){
	if(!p) return n;
	if (element_compare(p, n) == -1){
		p->left = element_insert(p->left, n);
		p->left->par = p;
	}
	else{
		p->right = element_insert(p->right, n);
		p->right->par = p;
	}
	return element_balance(p);
}

struct element_t* element_get_iter(struct element_t *e){
	if (!e) return NULL;
	while(e->left) e = e->left;
	return e;
}

struct element_t* element_iter_step(struct element_t*e){
	if (e->right){
		e = e->right;
		while(e->left) e = e->left;
		return e;
	}
	if (!(e->par)) return NULL;
	while(e->par){
		if (e->par->left == e){
			return e->par;
		}
		e = e->par;
	}
	return NULL;
}

char element_in(struct element_t *p, struct element_t *e){
	if (!p) return 0;
	char r;
	while(p){
		r = list_compare(p->el, e->el);
		if (!r) return 1;
		if (r == 1) p = p->right;
		else p = p->left;
	}
	return 0;
}


struct element_t* element_find(struct element_t *p, struct element_t *e){
	if (!p) return NULL;
	char r;
	while(p){
		r = list_compare(p->el, e->el);
		if (!r) return p;
		if (r == 1) p = p->right;
		else p = p->left;
	}
	return NULL;
}

void element_del(struct element_t *e){
	if (!e) return;
	if (e->left) element_del(e->left);
	if (e->right) element_del(e->right);
	list_del(e->el);
	statue_list_del(e->sts);
	free(e);
}

void reg_find_memory(reg_t *r, ecv_class_t *head, ecv_class_t *tail, ecv_class_t **stoc, int size){
	ecv_class_t **class_mas = (ecv_class_t**)malloc(sizeof(ecv_class_t*)*size);
	int i = 0, j, step = 0, max = (size*(size-1))/2;
	int res = 0;
	ecv_class_t *tmp = head;
	struct element_t *tmpel, *now = NULL, *next = NULL, *tm, *pmt;
	struct statue_list_t *tmpst, *pmtst;
	int *map_class, *map_out;
	map_class = (int*)malloc(sizeof(int)*size*2);
	map_out = (int*)malloc(sizeof(int)*size*2);
	i = 0;
	while (tmp){
		class_mas[i] = tmp;
		tmp->index = i;
		tmp = (ecv_class_t*)tmp->next;
		i++;
	}
	for (i = 0; i < size; i++){
		reg_set_st(r, class_mas[i]->sts[0]);
		map_out[size + i] = reg_takt(r, 1);
		map_class[size + i] = stoc[reg_show_st(r)]->index;

		reg_set_st(r, class_mas[i]->sts[0]);
		map_out[i] = reg_takt(r, 0);
		map_class[i] = stoc[reg_show_st(r)]->index;
	}
	now = element_malloc();
	for (i = 0; i < size; i++){
		element_add_st(now, i);
	}
	for (step = 0; step < max; step++){
		tmpel = element_get_iter(now);
		res = 1;
		while(tmpel){
			tmpst = tmpel->sts;
			while(tmpst){
				tm = element_copy(tmpel);
				element_add_step(tm, 0, map_out[tmpst->index], step);
				if (!element_in(next, tm)){
					next = element_insert(next, tm);
				}
				else{
					pmt = element_find(next, tm);
					element_del(tm);
					tm = pmt;
				}
				pmtst = tm->sts;
				while(pmtst){
					if (pmtst->index == map_class[tmpst->index]) break;
					pmtst = pmtst->next;
				}
				if (!pmtst){
					if (tm->sts) res = 0;
					element_add_st(tm, map_class[tmpst->index]);
				}


				tm = element_copy(tmpel);
				element_add_step(tm, 1, map_out[size + tmpst->index], step);
				if (!element_in(next, tm)){
					next = element_insert(next, tm);
				}
				else{
					pmt = element_find(next, tm);
					element_del(tm);
					tm = pmt;
				}
				pmtst = tm->sts;
				while(pmtst){
					if (pmtst->index == map_class[size + tmpst->index]) break;
					pmtst = pmtst->next;
				}
				if (!pmtst){
					if (tm->sts) res = 0;
					element_add_st(tm, map_class[size + tmpst->index]);
				}
				tmpst = tmpst->next;
			}
			tmpel = element_iter_step(tmpel);
		}
		element_del(now);
		now = next;
		next = NULL;
		printf("step: %d\n", step + 1);
		tmpel = element_get_iter(now);
		while(tmpel){
			list_print(tmpel->el);
			tmpst = tmpel->sts;
			while(tmpst){
				printf("%x, ", class_mas[tmpst->index]->sts[0]);
				tmpst = tmpst->next;
			}
			printf("\n");
			tmpel = element_iter_step(tmpel);
		}
		printf("--------------------------------------------------\n");
		if (res) break;
	}
	element_del(now);
	free(class_mas);
	free(map_class);
	free(map_out);
}

//------------------------------------------------------------------------------------------------------

void floid_uorshal(char *mas, int size){
	int i, j, k;
	for (i = 0; i < size; i++){
		for (j = 0; j < size; j++){
			for (k = 0; k < size; k++){
				if (mas[j*size + i] && mas[i*size + k]){
					mas[j*size + k] = 1;
				}
			}
		}
	}
}

void reg_connect(reg_t *r){
	int size = 1 << r->size;
	char *mas = (char*)malloc(sizeof(char) * size * size);
	int i, j = 0;
	for (i = 0; i < size; i++){
		for (j = 0; j < size; j++){
			mas[i*size + j] = 0;
		}
	}
	for (i = 0; i < size; i++){
		reg_set_st(r, i);
		reg_takt(r, 0);
		j = reg_show_st(r);
		mas[i*size + j] = 1;
		mas[j*size + i] = 1;

		reg_set_st(r, i);
		reg_takt(r, 1);
		j = reg_show_st(r);
		mas[i*size + j] = 1;
		mas[j*size + i] = 1;
	}
	floid_uorshal(mas, size);
	int *check = (int*)malloc(sizeof(int)*size);
	for (i = 0; i < size; i++){
		check[i] = 1;
	}
	for (i = 0; i < size; i++){
		if (check[i]){
			check[i] = 0;
			printf("{%x, ", i);
			for (j = i + 1; j < size; j++){
				if (mas[i*size + j] && mas[j*size + i]){
					printf("%x, ", j);
					check[j] = 0;
				}
			}
			printf("}\n");
		}
	}
	free(mas);
	free(check);
}


void reg_connect_power(reg_t *r){
	int size = 1 << r->size;
	char *mas = (char*)malloc(sizeof(char) * size * size);
	int i, j = 0;
	for (i = 0; i < size; i++){
		for (j = 0; j < size; j++){
			mas[i*size + j] = 0;
		}
	}
	for (i = 0; i < size; i++){
		reg_set_st(r, i);
		reg_takt(r, 0);
		j = reg_show_st(r);
		mas[i*size + j] = 1;


		reg_set_st(r, i);
		reg_takt(r, 1);
		j = reg_show_st(r);
		mas[i*size + j] = 1;
	}
	floid_uorshal(mas, size);
	int *check = (int*)malloc(sizeof(int)*size);
	for (i = 0; i < size; i++){
		check[i] = 1;
	}
	for (i = 0; i < size; i++){
		if (check[i]){
			check[i] = 0;
			printf("{%x, ", i);
			for (j = i + 1; j < size; j++){
				if (mas[i*size + j] && mas[j*size + i]){
					printf("%x, ", j);
					check[j] = 0;
				}
			}
			printf("}\n");
		}
	}
	free(mas);
	free(check);
}

#endif
