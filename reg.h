#ifndef REG_H
#define REG_H

struct reg_t;

struct reg_t* reg_malloc();
void reg_init(struct reg_t*, unsigned char**, unsigned char**, unsigned char);
void reg_set_st(struct reg_t*, unsigned int);
unsigned int reg_show_st(struct reg_t*);
void reg_del(struct reg_t*);
unsigned char reg_takt(struct reg_t*, unsigned char);

void reg_find_min(struct reg_t*);

void reg_connect(struct reg_t*);
void reg_connect_power(struct reg_t*);

#endif
