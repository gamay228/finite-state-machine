#ifndef START_C
#define START_C

#include "reg.h"
#include <stdlib.h>
#include <stdio.h>

unsigned int glob;
int psi_mas[] = {-1};
int fi_mas[] = {-1};


unsigned char x(unsigned char i){
	return (glob>>i)&1;
}

unsigned char f_fi(unsigned int st, unsigned char z){
	glob = st;
	return x(7) ^ x(7)*x(5) ^ x(6)*x(3) ^ x(4)*x(3)*x(1) ^ x(5)*x(0) ^ z;
	for (int i = 0; fi_mas[i] != -1; i++) if (fi_mas[i] == st) return 1;
	return 0;
}

unsigned char f_psi(unsigned int st, unsigned char z){
	glob = st;
	return x(7)*x(5) ^ x(6)*x(0) ^ x(4)*x(3)*x(1)*x(0) ^ x(3)*x(1);
	for (int i = 0; psi_mas[i] != -1; i++) if (psi_mas[i] == st) return 1;
	return 0;
}


int main(int argc, char *argv[]){
	int size = 8;
	struct reg_t *r = reg_malloc();
	unsigned char **f = (unsigned char**)malloc(2*sizeof(unsigned char*));
	f[0] = (unsigned char*)malloc(sizeof(unsigned char) * (1 << size));
	f[1] = (unsigned char*)malloc(sizeof(unsigned char) * (1 << size));
	unsigned char **p = (unsigned char**)malloc(2*sizeof(unsigned char*));
	p[0] = (unsigned char*)malloc(sizeof(unsigned char) * (1 << size));
	p[1] = (unsigned char*)malloc(sizeof(unsigned char) * (1 << size));
	int i;
	for (i = 0; i < 1<<size; i++){
		f[0][i] = f_fi(i, 0);
		f[1][i] = f_fi(i, 1);
		p[0][i] = f_psi(i, f_fi(i, 0));
		p[1][i] = f_psi(i, f_fi(i, 1));
	}

	reg_init(r, f, p, size);
	reg_find_min(r);
//	reg_connect(r);
//	printf("--\n");
//	reg_connect_power(r);

	free(p[0]);
	free(p[1]);
	free(p);
	free(f[0]);
	free(f[1]);
	free(f);
	reg_del(r);
}

#endif
